# Tinymine
A mine search game for the Thumby console. Find all the gold.

Current state: released to Thumby arcade

## First, you need a Thumby
It's an extremely small game console.
https://thumby.us

## To run this
1. Plug your Thumby into your computer via USB
1. Open the Thumby code editor - https://code.thumby.us
1. Click "Arcade"
1. Find Tinymine
1. Click "Add to Thumby"

Now you should be able to play the game on your Thumby.

### To run in the emulator
You don't even need a Thumby for this! Do the same as for loading onto a Thumby, but click "Load" instead of "Add to Thumby". If it doesn;t work and you see "Ch0ose mode", choose Python, then try "Load" again.

## Music
The tunes played by this game are in the format produced by TinyTunes. If you'd like to play a tune in a similar way, create and save it in TinyTunes, then extract the saved data from `/Saves/TinyTunes/persistent.json`. The file `playtune.py` is used to play a tune, something like this:
```
from playtune import Tune

Tune( myTuneString ).play()
```

## Thread usage
Tunes are played on a second thread - Thumby has two cores! But in the emulator, music will pause the game while it plays because starting a thread made the whole code editor non-functional.

If you want to do something similar in another game, look at `taskrunner.py` which starts a second thread and waits for tasks to appear for it to execute. I found that tasks that write to a file could be problematic: sometimes the Thumby would become unresponsive. I assume there was a deadlock between the threads, even though file access was protected by locks. Obviouly I could have been doing something wrong. `8~)`

To play a tune on the other thread, do something like this:
```
from playtune import Tune
from taskrunner import startTaskRunner, addTask
startTaskRunner( '/Games/MyGame' )

# When it's time to play a tune:
addTask( Tune( myTuneString ).play )
```
